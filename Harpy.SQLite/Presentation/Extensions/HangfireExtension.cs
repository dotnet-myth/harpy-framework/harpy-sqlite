﻿using Hangfire.SQLite;
using System;

namespace Harpy.SQLite.Presentation.Extensions
{

    public static class HangfireSQLite {

        public static SQLiteStorage UseStorage( string hangfireConnection = null ) {
            if ( string.IsNullOrEmpty( hangfireConnection ) )
                hangfireConnection = "Data Source=:memory:;";

            var options = new SQLiteStorageOptions {
                TransactionTimeout = TimeSpan.FromMinutes( 15 ),
                JobExpirationCheckInterval = TimeSpan.FromMinutes( 15 )
            };

            return new SQLiteStorage( hangfireConnection, options );
        }
    }
}
