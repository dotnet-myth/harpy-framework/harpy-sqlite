﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Data;

namespace Harpy.SQLite.IoC
{

    public static class SqliteContainer {

        public static IDbConnection UseSqliteConnection( this DbContextOptionsBuilder dbContextOptions, string connectionString = null, Action<SqliteDbContextOptionsBuilder> sqliteOptionsAction = null ) {
            if ( string.IsNullOrEmpty( connectionString ) )
                connectionString = "Data Source=:memory:";

            var dbConnection = new SqliteConnection( connectionString );
            dbConnection.Open( );

            dbContextOptions.UseSqlite( dbConnection, sqliteOptionsAction );

            return dbConnection;
        }

        public static SqliteDbContextOptionsBuilder AddMigrations( this SqliteDbContextOptionsBuilder optionsBuilder, string migrationsAssembly = "" ) {
            return optionsBuilder
                .MigrationsAssembly( migrationsAssembly )
                .MigrationsHistoryTable( "migrations_history" );
        }
    }
}
